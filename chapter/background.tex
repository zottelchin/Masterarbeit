\chapter{Background}\label{ch:background}

As this work touches upon different topics that are somewhat specialised, this chapter is meant to give a brief understanding of these topics. Starting with configurable software systems and the preprocessor cpp, this is followed by the concept of association rule mining.

\section{Configurable Software Systems}\label{ch:background:css}

This work is highly focused on on configurable software systems. The concept of configurable software systems is as broad as the name makes it sound. A software systems counts as configurable, if its behavior and functions can be changed so the software runs \textit{differently}, matching the requirements of the deployment. Such changes can be mandatory to run the software on different hardware (different system architectures as Arm and x64), OSs (like Windows or macOS) or User Interfaces (the chosen desktop manager on linux for example, or proprietary interfaces). In other cases, the configuration options are implemented to allow the software to adapt to the IT-Systems running alongside, such as different database management systems.

\begin{figure}[hbt]
 \centering
  \includegraphics[width=.8\textwidth]{diagramms/config-stages.png}
  \caption{Possible Stages of configuration in software}
\end{figure}


All these configuration options can be implemented to be set at two stages in the software development and deployment process. The first stage is the compilation of source code into an executable for the operating system. The second stage is the software system's runtime (e.g. with a configuration file or environment variables). Often both stages are used together, each for different configuration options, as they have advantages and disadvantages over the other, which are not discussed in this work. 

In this work the configuration at compile time is the main focus and used for the analysis. For this stage of configuration there exist two different approaches. Namely these are the annotation based approach (which will be discussed in this work and can be seen in \autoref{lst:helloworld}) and the composition based approach. In the annotation based approach, parts of the code are marked as a feature that can programmatically be removed to generate a variation of the software. The other approach uses a composition of separated modules to add or remove features from the software and is therefore called composition based approach. These approaches are independent from the language as well as the software used for the processing and can be adapted in a number of ways. \cite{Benduhn2016MigrationFA}

The focus in this work lies on the annotation based approach and especially the usage in the C programming language. To process the annotations before the build of the software, a preprocessor is used. The preprocessor is given a list of configuration options to enable (like a boolean set to TRUE; some preprocessors also accept data as values) and the annotated source code to process. By matching a special syntax in the source code, only a subset of the source code is given to the compiler to translate from human readable to computer interpretable.

\begin{figure}[hbt]
  \centering
  \includegraphics[width=.6\textwidth]{diagramms/preprocessor.png}
  \caption{Example of annotated code and the processed result}
  \label{lst:helloworld}
\end{figure}


A prominent example for annotation-based variability is the C preprocess \textit{cpp}\footnote{Documentation for cpp: \url{https://gcc.gnu.org/onlinedocs/cpp/Overview.html\#Overview}}. cpp stands for C preprocessor as it is developed to be used with C code and C code variations like C++ and Objective-C, and comes bundled in the gcc package \cite[Ch. 1 Overview]{cpp-docs}. The preprocessor was introduced to the c programming language in the early 1970s. One of the driving forces behind the introduction was Alan Snyder, who was working on a portable C compiler \cite{Snyder1975APC} and had an interest in the feature for his work. Another for was the \enquote{recognition of the utility of the the file-inclusion mechanisms available in BCPL and PL/I} \cite{10.1145/234286.1057834}. The first version only supported the inclusion of other files and simple replacement of strings and was only extended in functionality over time. \cite{10.1145/234286.1057834}

The cpp preprocessor has many features like macro definition or file inclusion, but the one important for this work is the conditional compilation. The main directives referenced in this work are \verb+#if defined()+, its shortened form \verb+#ifdef+ and the alternative branches \verb+#else+ and \verb+#elif+ (combination of else and if into one directive). Every \verb+if+ has to be closed by an \verb+#endif+, which also incloses optional \verb+else+ and nested \verb+if+ statements. A small example with cpp statements is provided in \autoref{lst:helloworld}.

There are also some critical aspects regarding the cpp preprocessor. One is the lack of discipline in using annotations in some projects. The placement of annotations can be aligned with the syntactic structure of the code, which is the case if a complete syntactical block of the source code is annotated. This code is referred to as disciplined by \citeauthor{10.5555/2541773}. In most projects e.g. the linux kernel, this is achieved by using conventions for the annotations placement and usage.
Other criticisms are that code regarding one feature is often scattered throughout the codebase and not as neatly separable as in a modular approach. This is not only reducing the code's readability but also has an negative impact on the configuration knowledge. \cite[Chapter 5.3]{10.5555/2541773}

 \citeauthor{Favre95thecpp} and \citeauthor{957833} \enquote*{[...] argue that preprocessors, due to their simplicity, invite developers to make ad hoc extensions and use “quick and dirty” solutions, instead of restructuring the code.} \cite[Chapter 5.3.6]{10.5555/2541773}. This can lead to \textit{chaotic} code structures with many different features but no overall design. Consequences are often high time consumption for maintenance and code that is difficult to unterstand. \cite[Ch 5.3]{10.5555/2541773}
 Another criticism targeting the understandability is the plain fact that the annotations add another language to the source files. 
 
 The use of preprocessors and features often also increases the frequency of syntax errors. "It is possible that these only occur in specific feature selections and therefore \textit{survive} until the specific configuration is used. As there are no built-in tools to check the syntax of every feature selection, this is often not caught before deployment \cite[Ch 5.3]{10.5555/2541773}.


\section{Association Rule Mining}\label{ch:background:arm}

Association rule mining started out in the retail industry. With digital point of sale systems, the information about transactions and the items purchased in a transaction were stored in a database. The data held great potential for the marketing of the stores as they could use the data to build improved marketing campaigns.
The fundamental association rule analysts were looking for in the data where: If a customer bought item X and item Y, would item Z also be purchased with a probability over a given threshold \cite{10.1145/170035.170072}.

The goal in the association rule mining is to identify all existing rules and know their probability. To accomplish this, the data is fed to an algorithm to first identify all associations in the dataset and then calculate their probability \cite{10.1145/170035.170072}. There exist many publications with different algorithms. They all have the same goal, but take different approaches to reach it, because they target different circumstances. Some algorithms for example specify that the collection of purchased items consists only of one item, while others allow different numbers of items up to the total number of distinct items.

 A specialization of association rule mining is the targeted association rule mining, which will not be covered in this work. The keyword \textit{targeted} can be simplified into filtered in this case. The algorithm takes an additional input and filters the dataset based on this input. This is useful to reduce computational cost and time if the desired item is known. Thus an analyst interested in Product X gets only the result containing X, because before the algorithm is ran, the input data was filtered for all transactions containing X. A transaction containing only A,B,C would not be processed \cite{7476643}.
 
% mögliche Quellen:
% https://dl.acm.org/doi/pdf/10.1145/170036.170072
% https://dl.acm.org/doi/pdf/10.1145/304181.304195
% https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.40.7506&rep=rep1&type=pdf

%\url{https://towardsdatascience.com/apriori-association-rule-mining-explanation-and-python-implementation-290b42afdfc6}
%\url{https://github.com/chonyy/apriori_python}
%\url{https://arxiv.org/ftp/arxiv/papers/1403/1403.3948.pdf}
%\url{https://www.geeksforgeeks.org/apriori-algorithm/}
%\url{https://medium.com/analytics-vidhya/association-rule-mining-concept-and-implementation-28443d16f611}


%Implementierung die als Grundlage für Apriori verwendet werden kann
%\url{https://link.springer.com/chapter/10.1007/978-3-030-49663-0_44}
%\url{https://github.com/terminal0gr/WebApriori}

%oder auch 
%\url{https://github.com/chonyy/apriori_python}
%\url{https://towardsdatascience.com/apriori-association-rule-mining-explanation-and-python-implementation-290b42afdfc6}

%oder auch
%\url{https://github.com/HonglingLei/Association-Rule-Mining}

%\url{https://www.kaggle.com/code/datatheque/association-rules-mining-market-basket-analysis/notebook}

%\url{https://dl.acm.org/doi/10.14778/2078324.2078330}
