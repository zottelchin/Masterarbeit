\chapter{Methodology}\label{ch:methodic}

This chapter gives a detailed description of the methodology I have developed to analyse the evolution of features. The chosen method for this task is a feature-oriented co-change analysis. This is split into two stages: the data aggregation and the data analysis. For these stages the methodical process is described, which will be conducted for a number of subject systems in  \autoref{ch:eval}. In the section about the data aggregation stage (\autoref{ch:methodic:s1}), the steps for extracting the relevant feature change data from a commit is described. Following that, in the data analysis stage in \autoref{ch:mehtodic:s2}, the approaches to analyse the extracted data are presented. Lastly, some more technical aspects of the implementation for the whole process are outlined. 

\section{Problem Statement}\label{sec:problem}

Preprocessors, such as the C preprocessor cpp, are a widely used configuration mechanism to create configurable or variable software systems. Depending on the selected configuration options (also referred to as features), a number of different variants of the software can be generated \cite{10.1007/s10270-015-0483-z}. In such systems, the configuration options are can in some cases be interdependent, which means that, certain configuration options are tangled with each other. For example feature \texttt{F} can be a \textit{child-feature} of \texttt{P} which normally translates to feature \texttt{F} only being available if feature \texttt{P} was enabled. This resembles an \textit{explicit} dependency between these two features. 

Dependencies between features don't have to be this obvious, not only to the user, but also the developer. For example, is it feasible that two features \texttt{A} and \texttt{B} exist in different parts of the project, which on the first glance are independent of each other. But in some cases, the features are used in a way that would break with certain configurations, creating a non-obvious dependency. These dependencies will be referred to as \textit{implicit} dependencies. Nonetheless could those features be changed randomly together in one commit and don't have any programmatic connection with each other. Then this is not a dependency. However it is also conceivable that they have to be changed together for the software to work as intended and by that are dependent features. 

\begin{multicols}{2}
\begin{lstlisting}[captionpos=b,caption={Example of an explicit dependency in form of child-parent dependency},escapeinside={(*}{*)}]
#ifdef P 
// ...
  #ifdef F
  // ...
  #endif // End of F
// ...
#endif // End of P

(*\phantom*
\end{lstlisting}

\columnbreak

\begin{lstlisting}[captionpos=b,caption={Example for implicit dependency showing two files with different features with no visible dependency}]
// File A
#ifdef A
  ...
#endif

//File C
#ifdef B
  ...
#endif
\end{lstlisting}
\end{multicols}

The problem with those \textit{implicit} dependencies between features is: they can be mandatory to be edited together, but this dependency is not obvious. If a project has those \textit{implicit} dependencies, there are a number of consequences, which can result in problems and these are often time consuming. First and foremost, the error susceptibility increases as there are more locations that need to be changed and remembered. For the same reasons, the maintenance work increases. Since both domain knowledge as well as feature dependencies are implicit, it is often not persistent and will, in most cases, increase the error susceptibility and maintenance effort. Some of these consequences were already mentioned in \autoref{ch:background:css} in the context of critical aspects regarding the cpp preprocessor

To overcome this problem of implicit dependencies, a change recommendation system can be used. This however needs the data about feature dependencies. To this end, an automated process needs to be developed to extract the dependency data in an automated fashion from the configurable software system and its version control system. 

\section{Feature-Oriented Co-Change Analysis}

As mentioned in the previous section, we need to identify the implicit feature dependencies in order to guide developers by means of change recommendation systems. In order to accomplish this, data has to be collected, processed, analysed and saved so that change recommendation systems could use it. All this parts are combined in one complete process as visualised in \autoref{fig:methodic:overview}.

\begin{figure}[hbt]
  \includegraphics[width=\linewidth]{diagramms/method-overview.png}
  \caption{Flow diagram of abstract dataflow for the methodology}
  \label{fig:methodic:overview}
\end{figure}

The process consists of two main steps that can be further divided. As illustrated in \autoref{fig:methodic:overview}, the two main steps are the \textit{Data Aggregation} and the \textit{Data Analysis}. A detailed description of the steps as well as the technical realization can be found in the following sections. In the next paragraphs an overview will be given.

The input for the complete process is the source code for the project to be analysed in the form of a version control repository. This is also the input for the first step, the \textit{Data Aggregation}. This step consist of the analysis of previous commits, filtering of relevant changed code sections containing preprocessor directives and the export of this extracted data into a database. 
The database is an intermediate data store to pipe the data to the second step, the \textit{Data Analysis}. Within this step, the data is processed to extract implicit feature dependencies. This dependency data is the main output of the process. The step, in which these implicit feature dependencies are identified, is the main co-change analysis.
 Alongside this analysis, the data is also lightly processed to extract some numbers about the system as metrics for evaluation purposes.
 
\section{Stage 1: Data Aggregation}\label{ch:methodic:s1}

In order to aggregate the required data from the provided source code, a repetitive task processes each commit in the target repository. This process consists of three steps, which will be explained in this section. These three steps, as vizualised in \autoref{fig:methodic:overview}, are the \textit{code analysis}, the \textit{filtering} for changed features and the \textit{export} of this data into a database. The goal of this data aggregation process is a list of changed features per commit.



\paragraph{Analysis}

This first step requires the source code repository of the system to be analysed  as an input. As the task of the analysis is repeated for every commit in the repository, the source code of a specific commit is the input for this task to be precise. This source code is then analysed for code annotations, especially for preprocessor statements, in order to locate all feature locations. For this task, the tool cppstats\footnote{GitHub repository of cppstats\url{https://github.com/clhunsen/cppstats/}} is used. 
cppstats can, among other things, analyse C source code with preprocessor annotations and generate a list of features and their locations in the files.

In order to reduce the processing time in this step, not the complete repository is processed, as only C files with changes in this commit contain relevant information. To retrieve only these files, the version control system is used. This step is further simplified by only processing new and modified lines as these can be found in the current state of the repository. To process deleted lines and their surrounding features, the change data and the previous file state have to be taken into account, which would increase the complexity of the analysis and probably double the processing time per commit, as the previous commit's state would have to be processed, too. This means that deleted lines inside of features are not taken into account in this analysis.
\textit{cppstats} returns a CSV file containing data of all affected feature annotations in the processed files, each with the following information: the file name, start and end line of the annotation, the annotation type (e.g. if), the expression and the constants in the expression.
After that, this data needs to be filtered, as only changed features are subject of this analysis.

\paragraph{Filtering}

To filter this data, all feature locations are intersected with data from the version control system. More precisely, every changed file will be processed line by line. For every changed line, the feature location data is consulted in oder to determine if this changed line belongs to a feature. If this is the case, the feature information is saved for export. If the line is not enclosed by feature annotations, the line is ignored and not further processed, as these lines do not belong to any feature. The implementation for this can be found in the already mentioned repository.

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{diagramms/Masterthesis-V2.drawio.png}
	\label{fig:overview}
	\caption{Schematic illustration of the data aggregation stage}
\end{figure}

\paragraph{Export}

The second half of the afore-mentioned script contains the code for the last part of the data aggregation stage. To save the data obtained in this stage, it needs to be exported into a persistent storage for later analysis. The data is saved in two different locations: the file system and a database. The file storage is realised through a CSV file with the same columns as the database. The database is the primary location in this setup and the local copy is for used for redundancy, if the database goes offline for example. As the analysis of all commits is time consuming, so reruns should be avoided whenever possible. 
 Along with the commit hash and feature constant(s), the file path and type and expression of the annotation are saved. This additional data is not relevant for this research but is provided and stored for usage in future research.

\section{Stage 2: Data Analysis}\label{ch:mehtodic:s2}

When the data aggregation is completed, the data is processed for analysis. This stage consist of two different approaches. In the first analysis, I compute a number of metrics about the repository itself. Among others, this includes the total number of commits with feature changes, changed feature count per commit and other similar metrics. Using this data, the results from the different subject systems can be compared and first assumptions regarding co-changes can be made. The results can give a first impression of the feature usage in the subject system. Are there features at all and do those features change over time? Relevant for the association rule mining is also: are there commits with changes in multiple features? This is also relevant, as only those can hint possible co-changes and association rules. 

The second analysis method uses a technique called association rule mining (see also \autoref{ch:background:arm}). Using this method, \textit{feature rules} are identified for features, which frequently change when another feature is changed as well. That way, implicit feature dependencies can be revealed. In this case the \textit{apriori} algorithm is used, as this is a rather simple algorithm for association rule mining with only two parameters in its original form \cite{10.1145/170036.170072}. In \autoref{fig:apriori}, I illustrate the application of this algorithm in a simplified manner.

\begin{figure}[hbt]
	\centering
  	\includegraphics[width=.9\textwidth]{diagramms/apriori}
  	\caption{Simplified illustration of the steps included in the apriori algorithm}
  	\label{fig:apriori}
\end{figure}

The general procedure of the algorithm will be described in the following paragraphs. In the paper where the algorithm was proposed by \citeauthor{10.1145/170036.170072}, it was used to extract association rules for items in a supermarket, which means that the names of the terms are mostly from this domain. In this work, some of the terms are changed to fit the domain of software and features. Mainly the term \textit{item} is changed to \textit{feature} and \textit{transaction} to \textit{feature set}.

As its main input the algorithm uses a list of feature sets. Feature set in this context stands for an array of features that were changed together in one commit - as an example $[Feature A, Feature C, Feature E]$ were changed in one commit together and now represent a feature set for this commit. This list of feature sets is used in a first step to extract a list of all distinct features. Because the complete history of the project was processed, the list of features is complete. In \autoref{fig:apriori} this list is displayed with the gear between the first and second data sheet. 

Based on this list of features, the next step is executed and all possible association rules are created. This starts with the first two features like $[Feature A] \rightarrow [Feature B]$ and end with $[Feature A, ..., Feature Y] \rightarrow [Feature Z]$ (assuming features A-Z). At this stage, some combinations of features are not logical in reality and there are different improvements proposed in different implementations and publications, but they are not relevant for the general understanding of the algorithm.

For each generated rule the two metrics \textit{confidence} and \textit{support} are calculated in order to evaluate the meaningfulness of this rule. The metrics also allow the comparison of different rules with each other. However, rules can only be compared within a project, as the base data has to be comparable. 
The metrics indicate the following for every rule:
 
 \begin{description}
 	\item [Confidence] The confidence is measured in percent, and is a measurement for the strength of a given rule. It indicates the number of feature sets that contain the consequent features of the association rule, when the antecedent features are included \cite[Chapter 2]{10.1145/170036.170072}. For example, if a rule states $[Feature A] \rightarrow [Feature B]$ with the confidence of 90\%. This means that $Feature B$ is included in 90\% of the feature sets also including $Feature A$.
 	\item [Support] The support is also measured in percent but \enquote{corresponds to statistical significance} \cite[Chapter 2]{10.1145/170036.170072} of the rule. It describes the ratio of feature sets containing both the antecedent as well as the consequent features. As an example, the association rule $[Feature A] \rightarrow [Feature B]$ references $FeatureA$ and $FeatureB$. So, if there are two feature sets containing both features and a total of ten feature sets, the support for the before mentioned rule is 20\%.
 \end{description}
 
 In addition to the feature sets as the main input, the algorithm has two parameters that can be adjusted. These parameters are the \textit{minimum confidence} and \textit{minimum support}. With these parameters, a threshold value is defined above which the corresponding values of a rule must lie to be included in the result \cite[Chapter 6]{10.1145/170036.170072}. If the rule does not comply with these criteria, it is eliminated. As displayed in \autoref{fig:apriori}, this filtering is done in two stages, first the support and then the confidence. Every rule that satisfies both of these parameters is included in the result set and is classified an association rule for the project.

\section{Technical Realisation}

The previous sections described the conceptual approach to tackle the feature-oriented co-change analysis conducted in this work. In this section the technical details about the implementation of my concept into a working system will be shared. Some of the used third party tools and own scripts were already mentioned and referenced in this chapter or the background (see \autoref{ch:background}). Others will be introduced in this section. 
Hereby only an overview will be given about the used tools, prerequirements and procedure. The complete source code as well as usage instructions can be found in the Git repository\footnote{Git repository with tooling and results: \url{https://codeberg.org/zottelchin/Masterthesis}}. 

At first, I provide some general details relevant for the entire process. Throughout the complete process unix based systems were used for testing and production purposes. As the majority of tools are packaged into Docker containers, this setup should be running on all major operating systems with support for Docker. As there were some problems with the Docker environment on macOS, I strongly advise to use a linux system (I used Debian). The decision for Docker as another abstraction layer has multiple reasons. Firstly, this helps with the overall management of the different steps, allowing a simple parallelization of the tasks and greatly improving the dependency management, as some of the required dependencies were older.

\begin{figure}[hbt]
	\centering
  \includegraphics[width=.5\linewidth]{diagramms/containers}
  \caption{Steps as Containers used in the data aggregation stage}
  \label{fig:methodic:tech:simply-container}
\end{figure}


For the data aggregation stage, the three steps in \autoref{fig:methodic:overview} can not directly be mapped to the four containers (see \autoref{fig:methodic:tech:simply-container}). The steps for change filtering and data export are combined into one Python script. The reason for this is, that the results of the filtering need to be saved anyways in a format that's easily insertable into a database. The data is not only written to the database, but also to a local file mounted into the container.

Two of the remaining containers prepare the files for \texttt{cppstats}. In this, the right repo state is checked out, the changed files are identified and the file structure for cppstats is created. As cppstats was developed for Python 2 and the current standard is Python 3, the container for this is based on Debian 10 (Codename: \texttt{Buster}) as this version ships with Python 2 by default. The required dependencies are also installed in matching version following the documentation\footnote{Installation instructions for cppstats: \url{https://github.com/clhunsen/cppstats/blob/master/INSTALL.md}}. The Dockerfile with all commands can be found in the aforementioned repository.

To run this analysis for large numbers of commits with improved system utilisation, multiple commits should be processed in parallel. Although the file analysis with cppstats is the step with the highest cpu usage, it is only utilizing one core. To improve the performance of that step, a small \texttt{workbalancer} was created  to process a list of commits with multiple parallel worker processes. This lead to a much better utilization of the available CPU resources, as the processes are now distributed over the available cores. 
The next bottleneck was the disk speed. In order to process multiple commits at the same time, every process needs its own working copy of the repository. This meant that for every commit, the complete repository was copied into a new folder. To improve the speed of this copy, the working directories and the repository are moved into RAM in preparation. This results in better performance, as the repository is copied from the RAM into another folder in the ram.

As the database system, PostgreSQL\footnote{Webpage of PostgreSQL: \url{https://www.postgresql.org/}} is used. This could be be changed to another relational database implementation with minor changes in the table creation command. All major database systems are also available as a Docker image that can simply be started in a dockerised environment. The used SQL statements are collected in the git repository\footnote{Git repository with tooling and results: \url{https://codeberg.org/zottelchin/Masterthesis}}.

The two steps in the data analysis stage visualised in \autoref{fig:methodic:overview} correspond to two scripts for the two analyses. The statistical analysis is implemented in a Python script. The analysis part is done with just build-in datatypes and some loops. The output is printed to the console for integration into other systems and is also saved to the disk as a text file. The dependency extraction, or more precisely the association rule mining, utilizes multi threading with Go as programming language. The implementation for the apriori algorithm is imported from an external library by \texttt{eMAGTechLabs} on GitHub\footnote{Apriori library by eMAGTechLabs: \url{github.com/eMAGTechLabs/go-apriori}}. The output is also presented in the console and saved into the database. 

\section{Chapter Summary}

In this chapter the problem was explained and the resulting task has been stated: the development of an automated process for feature dependency data. For this, the methodology was presented in the following sections. The proposed solution is a feature-oriented co-change analysis consisting of two stages. In a data aggregation stage, the source code is processed and change information about features is extracted. This data is analysed in a second stage to identify association rules using an apriori algorithm for association rule mining. 
Lastly, relevant aspects of the technical implementation are presented - as a summary, the complete process can be run in a dockerized environment. Some third party tools like cppstats (preprocessor annotation analysis) are used, complimented with own scripts and tools to accomplish this task. The next chapter contains the documentation of the conduct for the co-change analysis. 


































