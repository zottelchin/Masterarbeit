% So apparently, a LaTeX update was so stupid to break special characters in packages, so congrats, we need the following command ...
\UseRawInputEncoding

\ProvidesClass{tex/thesisJacob}[2020/09/03 Jacob Krüger]
\LoadClass[12pt,titlepage]{book}
% Set:
%		final to remove todos
%		print to have uncolored hyperref links
%		german to switch language to German

\usepackage{etoolbox}
\newtoggle{final}
\newtoggle{print}
\newtoggle{german}

\DeclareOption{final}{\toggletrue{final}}
\DeclareOption{print}{\toggletrue{print}}
\DeclareOption{german}{\toggletrue{german}}
\ProcessOptions\relax

\iftoggle{final}{
	\newcommand{\todo}[1]{}
}{
	\newcommand{\todo}[1]{{\color{red} \emph{#1}}}
}

\iftoggle{german}{
	\usepackage[ngerman]{babel}
}{
	\usepackage[english]{babel}
}

\usepackage{marginnote}
\usepackage{setspace}
% For some authornames
\usepackage{combelow}

\usepackage{pdfpages}
\newcommand{\signed}{	
	\includepdf{docs/signed.pdf}
	\blankpage
}

% Fonts and style
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{apple_emoji}

\usepackage[a4paper]{geometry}
\geometry{left=30mm,right=30mm,top=30mm,bottom=25mm,head=14.5pt,marginparwidth=20mm, marginparsep=5mm}

\parindent 0pt
\parskip 1.5ex plus 05ex minus 0.5ex

\clubpenalty = 10000
\widowpenalty = 10000
\displaywidowpenalty = 10000

\newcommand{\blankpage}{\clearpage{\pagestyle{empty}\cleardoublepage}}

\usepackage{fancyhdr}
\parindent 0pt
\parskip 1.5ex plus 0.5ex minus 0.5ex

\newcommand{\headfont}{\fontfamily{ppl}\selectfont}
\newcommand{\captionfont}{}
\newcommand{\chapterheadfont}{}

\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{\thechapter.\ #1}{}}
\fancyhf{}
\fancyhead[LE,RO]{{\headfont\thepage}}
\fancyhead[LO]{\headfont\nouppercase{\rightmark}}
\fancyhead[RE]{\headfont\nouppercase{\leftmark}}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\fancypagestyle{plain}{
	\fancyhf{}
	\renewcommand{\headrulewidth}{0pt}
	\renewcommand{\footrulewidth}{0pt}
}

\renewcommand{\chaptername}{}

\renewcommand{\section}{
	\@startsection{section}
		{1}										% level
		{0pt}									% indent
		{1.5ex plus 1ex minus 1.2ex}			% before
		{0.5ex plus 0.5ex minus 0.5ex}			% after
		{\chapterheadfont\Large\bfseries}		% style
}

\renewcommand{\subsection}{
	\@startsection{subsection}
		{2}
		{0mm}
		{1ex plus 1ex minus 1ex}
		{0.3ex plus 0.3ex minus 0.3ex}
		{\chapterheadfont\large\bfseries}
}

\renewcommand{\subsubsection}{
	\@startsection{subsubsection}
		{3}
		{0mm}
		{1ex plus 1ex minus 1ex}
		{0.2ex plus 0.2ex minus 0.2ex}
		{\chapterheadfont\normalsize\bfseries}
}

\renewcommand{\paragraph}{
	\@startsection{paragraph}
		{4}
		{0mm}
		{2ex plus 1ex minus 2ex}
		{0.2ex plus 0.2ex minus 0.2ex}
		{\chapterheadfont\normalsize\bfseries}
}

\newlength{\chapnolen}
\newlength{\chapparlen}
\newsavebox{\chapno}
\renewcommand{\@makechapterhead}[1]{
	\vspace*{0.2\textheight}
	\vskip 15\p@
	{\parindent \z@ \raggedright \normalfont
		\ifnum \c@secnumdepth >\m@ne
		\if@mainmatter
		\savebox{\chapno}{\chapterheadfont\huge\bfseries \thechapter.}
		\settowidth{\chapnolen}{\usebox{\chapno}}
		\parbox[t]{\chapnolen}{\usebox{\chapno}}\nobreak\leavevmode
		\fi
		\fi
		\interlinepenalty\@MM
		\setlength{\chapparlen}{\textwidth}
		\addtolength{\chapparlen}{-1.0\chapnolen}
		\addtolength{\chapparlen}{-2ex}
		\leavevmode\nobreak
		\parbox[t]{\chapparlen}{\raggedright\chapterheadfont\huge \bfseries #1\par\nobreak}
		\vskip 40\p@
	}
}

\renewcommand{\@makeschapterhead}[1]{
	\vspace*{50\p@}
	{\parindent \z@ \raggedright
		\normalfont
		\interlinepenalty\@M
		\chapterheadfont \huge \bfseries  #1\par\nobreak
		\vskip 40\p@
	}
}

% more packages
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage[inline]{enumitem}
%\setlist{leftmargin=3em}
\usepackage{url}
\def\UrlFont{\rm}
\usepackage{adjustbox}
\usepackage{multirow}
\usepackage{subcaption}
\usepackage{makeidx}
\usepackage{listings}
\usepackage[super]{nth}
\usepackage{xspace}
\usepackage[babel]{csquotes}
\usepackage[shortcuts]{extdash}

\usepackage{fncylab}

\iftoggle{german}{
	\renewcommand{\lstlistingname}{Quelltext}
}{
	\renewcommand{\lstlistingname}{Listing}	
}

% Define colors and their commands
\newcommand{\colorFont}[2]{{\color{#1}#2\xspace}}
\definecolor{myGray}{RGB}{128,128,128}

% Referencing
\usepackage[notindex,nottoc]{tocbibind}
\iftoggle{german}{
	\renewcommand{\lstlistlistingname}{Quelltextverzeichnis}
}{
	\renewcommand{\lstlistlistingname}{List of Code Listings}
}
\renewcommand{\lstlistoflistings}{\begingroup
	\tocfile{\lstlistingname}{lol}
\endgroup}

\lstset{frameround=fttt}

\definecolor{pdflinkcolor}{RGB}{0,57,97}
\definecolor{pdfcitecolor}{RGB}{6,116,0}
\usepackage
	[raiselinks=true,
	bookmarks=true,
	bookmarksopenlevel=2,
	bookmarksopen=true,
	bookmarksnumbered=true,
	pagebackref=true,
	hyperindex=true,
	\iftoggle{print}{hidelinks,}{colorlinks=true,}
	linkcolor=pdflinkcolor,
	anchorcolor=pdflinkcolor,
	citecolor=pdfcitecolor,
	filecolor=pdflinkcolor,
	menucolor=pdflinkcolor,
	urlcolor=pdflinkcolor,
	plainpages=false,
	pdfpagelabels=true]
		{hyperref}

% autoref naming definition for englisch and german

\addto\extrasngerman{\def\chapterautorefname{Kapitel}}
\addto\extrasngerman{\def\sectionautorefname{Abschnitt}}
\addto\extrasngerman{\def\subsectionautorefname{Abschnitt}}
\addto\extrasngerman{\def\subsubsectionautorefname{Abschnitt}}

\addto\extrasenglish{\def\chapterautorefname{Chapter}}
\addto\extrasenglish{\def\sectionautorefname{Section}}
\addto\extrasenglish{\def\subsectionautorefname{Section}}
\addto\extrasenglish{\def\subsubsectionautorefname{Section}}


\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

% Table of Contents

\let\@olddottedtocline\@dottedtocline
\renewcommand{\@dottedtocline}[5]{\@olddottedtocline{#1}{#2}{#3}{#4}{\chapterheadfont #5}}

% Titlepage
\def\titlehead#1{\def\@titlehead{#1}}
\def\titlefoot#1{\def\@titlefoot{#1}}
\def\@titlehead{}
\def\@titlefoot{}

\def\ps@title{%
	\headheight 15mm
	\footskip   0cm
	\addtolength{\textheight}{-15mm}
	\let\@mkboth\@gobbletwo%
	\def\@oddhead{\vbox{\hbox to\textwidth{\@titlehead}
			\vskip 1.5mm
			\hbox to\textwidth{\hrulefill}}}
	\def\@oddfoot{\vbox{\vskip -1mm\hbox to\textwidth{\hrulefill}\vskip 1mm
			\hbox to\textwidth{\@titlefoot}}}
	\let\@evenhead\@oddhead
	\let\@evenfoot\@oddfoot
}

\renewenvironment{titlepage}
{
	\let\oldheadheight\headheight
	\let\oldfootskip\footskip
	\let\oldtextheight\textheight
	
	\cleardoublepage
	\if@twocolumn
	\@restonecoltrue\onecolumn
	\else
	\@restonecolfalse\newpage
	\fi
	\thispagestyle{title}
	\setcounter{page}\@ne
}
{\if@restonecol\twocolumn \else \newpage \fi
	\if@twoside\else
	\setcounter{page}\@ne
	\fi
	\let\headheight\oldheadheight
	\let\textheight\oldtextheight
	\let\footskip\oldfootskip
}

\iftoggle{german}{
	\newcommand{\acknowledgmentname}{Danksagung}
}{
	\newcommand{\acknowledgmentname}{Acknowlagment}
}

% Bibliography
\usepackage[sort]{natbib}
\iftoggle{german}{
	\renewcommand{\bibname}{Literaturverzeichnis}
	\bibliographystyle{tex/plainnat-ger}
}{
	\renewcommand{\bibname}{Bibliography}
	\bibliographystyle{tex/plainnat}
}
\bibpunct{[}{]}{;}{a}{,}{,}

\usepackage{varioref}
\renewcommand*{\backref}[1]{}

\iftoggle{german}{
	\renewcommand*{\backrefalt}[4]{\footnotesize\color{gray}(zitiert auf Seite~#2)}
}{
	\renewcommand*{\backrefalt}[4]{\footnotesize\color{gray}(cited on Page~#2)}
}

\usepackage{tikz}

\newcommand{\logo}{\includegraphics[trim=0mm 0mm 50mm 0mm,clip,height=3cm]{INF_SIGN_druck.pdf}}

% Dockerfile lstlisting from https://github.com/marekaf/docker-lstlisting/blob/master/latex.tex
\lstdefinelanguage{docker}{
  keywords={FROM, RUN, COPY, ADD, ENTRYPOINT, CMD,  ENV, ARG, WORKDIR, EXPOSE, LABEL, USER, VOLUME, STOPSIGNAL, ONBUILD, MAINTAINER},
  keywordstyle=\color{blue}\bfseries,
  identifierstyle=\color{black},
  sensitive=false,
  comment=[l]{\#},
  commentstyle=\color{gray}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  morestring=[b]',
  morestring=[b]"
}

\usepackage{ltablex}

\newcommand{\specialcell}[2][c]{%
  \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}
  
\usepackage{multicol}
