\chapter{Results}\label{ch:results}

This chapter contains the results and findings from both analyses performed on the data. The raw data as well as the complete results from the analyses are available on the source code hosting site Codeberg\footnote{Git repository with tooling and results: \url{https://codeberg.org/zottelchin/Masterthesis}}. The chapter is split into two sections according to the two analyses. Starting with the statistical analysis, presenting an overview over each subject system as well as an overall view on the data. Followed by the results of the association rule mining, also presented by subject system.

\section{Results of the Co-Change Analysis}

This section starts with an overview of some numbers regarding the commits. In \autoref{tab:overview-res} the total number of commits is set into perspective with the number of commits with changes in a feature, named \textit{feature commits}. The discrepancy between the different subject systems is notable. The \textit{openvpn} repository has the highest share of feature commits with 52,9 \% and the lowest was found in the \textit{linux kernel} with 10,6 \%. As the systems in \autoref{tab:overview-res} are sorted by the number of total commits, it can be observed, that the feature share decreases over the subject systems, as the number of total commits increases. Assessing an dependency between the increasing commit count and decreasing feature share is difficult at this stage, as the data was only generated for five selected systems. 

\begin{tabularx}{\textwidth}{l r r r}
	\hline
	System & Total Commits & Features Commits\footnote{Feature Commit: A commit with changes in a Feature} & Feature Share\footnote{Feature Share: Share of commits with feature changes in proportion to the total number of commits. Rounded to one decimal.} \\
	\hline
	\hline
	openvpn & 3 176 & 1 681 & 52,9 \% \\ \hline
	libxml2 & 5 321 & 2 632 & 49,5 \% \\ \hline
	openldap & 23 985 & 7 762 & 32,4 \% \\ \hline
	postgres & 53 695 & 6 399 & 11,9 \% \\ \hline
	linux kernel & 1 090 089 & 115 558 & 10,6 \% \\ \hline
	\caption{Overview over subject systems with a comparison between total commits and commits with feature changes}
	\label{tab:overview-res}
\end{tabularx}

In addition to the commit counts the data also allows some insight into the feature side of the repositories. For every feature in a repository, the total number of distinct appearances in commits can be counted. As one repository can have a high number of features, a complete list of them would be huge and would not help in answering the research questions and is therefore presented in condensed form in \autoref{tab:overview-res-feat}. This table contains the number of distinct features per system, as well as a split between features only occuring in one commit and features found in multiple commits. The explainations behind features only occuring once in the change data can be numerous. But it is worth mentioning that the number of features occuring only once is not that small in comparison to the total number of features. 

If comparing the number of commits across the different systems in \autoref{tab:overview-res} and the total number of features in \autoref{tab:overview-res-feat}, the subject systems \textit{postgres} and \textit{openldap} are noticeable. They both have a similar number of features but \textit{postgres} has over double the commits compared to \textit{openldap}. As \textit{postgres} has also nearly triple the lines of C code (for lines of code compare to \autoref{tab:subjectsystems}), this is a sign for further inverstigating the differences in the usage of features between those two systems. 

\begin{tabularx}{\textwidth}{l r r r }
	\hline
	System & \specialcell{Total\\distinct Features\footnote{Feature Count: Total number of distinct features found over commits using change data}} & Changed Once & \specialcell{Changed more\\than Once} \\
	\hline
	\hline
	openvpn & 425 & 107 & 318 \\ \hline
	libxml2 & 509 & 158 & 351 \\ \hline
	openldap & 1 300 & 426 & 874\\ \hline
	postgres & 1 340 & 311 & 1 029 \\ \hline
	linux kernel & 18 140 & 7395 & 10 745\\ \hline
	\caption{Overview over subject systems with total number of distinct features and features split between changed once and more than once}
	\label{tab:overview-res-feat}
\end{tabularx}

\begin{tabularx}{\textwidth}{l r r r r}
	\hline
	System & \specialcell{Feature\\Commits\footnote{Feature Count: Total number of distinct features found over commits using change data}} & \specialcell{Single Feature} & \specialcell{Multiple Changes} & \specialcell{Ratio\\Multiple Changes}  \\
	\hline
	\hline
	openvpn & 1 681 & 530 & 1 151 & 68\% \\ \hline
	libxml2 & 2 632 & 1 411 & 1 221 & 46\% \\ \hline
	openldap & 7 762 & 5 020 & 2 742 & 35\% \\ \hline
	postgres & 6 399 & 4 026 & 2 373 & 37\% \\ \hline
	linux kernel & 115 558 & 84 566 & 30 992 & 26\% \\ \hline
	\caption{Overview over subject systems with count of feature commits and split of those with changes in just one feature and more than one feature}
	\label{tab:overview-res-commits}
\end{tabularx}

In \autoref{tab:overview-res-commits} the feature commits split into commits with changes in only one feature and commits with changes in multiple features are listed. Multiple feature changes can occur multiple ways with the simplest being two separate files with two different features. Another option would be, that the features are separate but in the same file. Last there is the use of nested features (one feature block is encapsulated in another feature block) or complex feature expressions, where multiple features are combined with logical operators. As cppstats retrieves the expression anyway, it is saved in the data store, as some future research may need it. An example for a complex feature expression and for a simple one are given in \autoref{lst:data-example} from the \textit{openldap} data.

\begin{lstlisting}[caption={A row from the database with results from openldap, seperated by ';'. This is a complex expression, as multiple features are involved.},captionpos=b,label={lst:data-example},frame=single]
# ID; Commit Hash; File; Type; Expression; Constants
10827; 
b4e37e518fa20227509b9bf464e1b6049ba82b0a; 
servers/slapd/back-sql/entry-id.c; 
#if; 
(defined(BACKSQL_TRACE)) && (defined(BACKSQL_ARBITRARY_KEY)); 
BACKSQL_ARBITRARY_KEY,BACKSQL_TRACE
\end{lstlisting}

\begin{lstlisting}[caption={A row from the database with results from openldap, seperated by ';'. This expression only consist of one feature and no logical connectors and is classified a simple one.},captionpos=b,frame=single]
# ID; Commit Hash; File; Type; Expression; Constants
10820;
b4e37e518fa20227509b9bf464e1b6049ba82b0a;
servers/slapd/back-sql/add.c;
#if;
defined(BACKSQL_ARBITRARY_KEY);
BACKSQL_ARBITRARY_KEY
\end{lstlisting}

As I presented in this section, the basic change data in combination with the feature data can give insides into the different subject systems. Some of these insights provide data for answering the research questions, that in every subject system there are features with multiple changes and also commits with changes in multiple features. This supports the idea that association rules can be found in this data, but it needs to be further processed to extract potential rules. The results of the association rule mining will be presented in the following section. These results will then allow me\todo{?} to answer the research questions.


\section{Results of the Association Rule Mining}

As the last section presented a more general view on the statistics for the different repositories, this section contains the results from the association rule mining. The methodology is described in \autoref{ch:mehtodic:s2} in general and in \autoref{ch:eval:meth} as to how it is conducted. As in the last section there will be an overview paragraph at first, followed by paragraphs for the different subject systems.

\autoref{tab:overview-apri} shows the total number of rules per subject system as well as the number of rules by rule extent. Rule extent in this context describes the number of features present in a rule. Rules are split by rule type between simple rules consisting of one feature pointing to another and complex rules where changes in multiple features advice a change in another feature. As described in \autoref{ch:mehtodic:s2}, a rule consist of two parts, the base set of features and the single feature to be added. Simple rules consist of one feature in the base set, while complex rules have multiple rules in the base set. An example for a complex rule would be $[Feature A, Feature B, Feature C] \rightarrow Feature F$. 

\begin{tabularx}{\textwidth}{r r r r r}
	\hline
	System & \specialcell{Distinct Features} & \specialcell{Number of\\ Rules} & Simple Rules\footnote{Simple Rule: If $Feature A$ only points to one associated feature, the rule is classified as simple} & Complex Rules\footnote{Complex Rule: If the feature change implies multiple additional feature changes} \\
	\hline
	\hline	
	libxml2 & 509 & 5896 & 11 & 5885 \\ \hline
	openvpn & 425 & 6356 & 49 &  6307 \\ \hline
	openldap & 1300 & 108 & 44 & 64 \\ \hline
	postgres & 1340 & 209 & 42 & 167 \\ \hline
	linux kernel & 18140 & 2477 & 280 & 2197 \\ \hline
	\caption{Overview of the number of rules by subject system including a split between simple and complex rules}
	\label{tab:overview-apri}
\end{tabularx}

As can be seen in \autoref{tab:overview-apri}, the number of rules found in the different subject systems varies strongly. In addition to the gap between the numbers the comparison of the number of features and the number of rules is very interesting. Postgres and openldap, both having feature commit counts in the six and seven thousand and around 1300 distinct features, have only 108 and 209 rules resulting from the analysis. While libxml2 and openvpn, with significantly smaller overall feature counts and feature commit numbers, have around 6000 rules.

These high numbers, especially for complex rules, have to be relativised a bit. If the features $A$, $B$ and $C$ form association rules, every combination of these three is tested for confidence und support. The results often include multiple combinations of the same features. On these rules the support is always the same, while the confidence can vary between the different combinations. This lies in the nature of these metrics. However this can result in higher numbers of total rules for systems with more distinct features and also in complex rules, as the number of feature combinations are increasing exponentially with the number of features. 

With improved or additional processing these rules could be consolidated with the loss of confidence for the individual rules. The confidence however is an important value for a change recommendation system as it helps the system to recommend the most likely change out of the possible changes, when more rules match the current changes. 
In the following paragraph the rules of the different systems are examined closely in order to answer the proposed research question, whether implicit dependencies can be found between the the features in the rules. 

\paragraph{libxml2}

libxml2, as one of the smaller subject systems by number of lines and number of commits, but has many co-changes of features that can be seen as rules. These rules have an average confidence of 95\% and the average for the support is 0,64\%. The average minimum support for this system was 0,57\%. 

Within nearly all rules, a feature with \verb+LIBXML+ in it can be found. Only very few have no feature with this substring in either part of the rule (10 and 13 of 5896 rules, with multiple duplicates between the two lists). As this is the system name and to this the names of the feature (e.g. \verb+LIBXML_CATALOG_ENABLED+) are added, this seems to be an internal naming convention.
Out of the rules without those features, the majority contains some subset of \verb+__CYGWIN__+, \verb+_WIN32+ and \verb+__DJGPP__+. CYGWIN\footnote{CYGWIN website: \url{http://cygwin.org/}} is, in simple words, a tool for developers to port their Linux software for Microsoft Windows. DJGPP\footnote{DJGPP website: \url{https://www.delorie.com/djgpp/}} is a tool to create executables for Windows. So both features reference tooling for creating windows executables, while the third features name states Windows 32 bit systems (shortened to WIN32). This indicates a strong logical link between these features.
 
Of the 5896 found rules 1132 contain the feature \verb+LIBXML_DEBUG_ENABLED+ on either side of the rule. This suggests that changes in features often require changes in the debug log part of the software. This is however one of the features where many changes in its code segments are not worrying, as changes and new additions in other features are often followed by changes in the debug output. This output seems to be controlled through a feature in this system.

\paragraph{openvpn}

The system openvpn is similar to libxml in regard to the high number of extracted rules. The rules average at about 98\% confidence and 3,93 \% support. The algorithm was limited with a minimum support of 0,89\%. 

In this system the majority of rules contain features with \verb+TARGET+ in the name, in contrast to the system name with libxml2. The prefix target resembles to the target system like Android, Windows or Darwin (MacOS) for example. As the rules in most cases point from one target system to another, it seems that changes in the software for one system often have to be conducted in the code for other operating systems. This makes sense as openvpn is a system built for different operating systems, which have different specific interfaces to the operating system. If some new functionality is added to the software, this often results in changes in multiple of the features, as different operating systems require different approaches.

\paragraph{openldap}

In contrast to the last two subject systems, in openldap few rules were found in the co-change analysis. The average confidence is similar to the other systems at about 97\%. The average support comes in at 0,31\% while the minimum support was set to 0,19\%.

The last systems had a dedicated identifier for the semantic of the features in the rules. Within openldap semantic naming this is not as present, but the relations between the features can be found nonetheless. For example, changes in \verb+LDBM_USE_BTREE+ and \verb+LDBM_USE_DBHASH+ can be found in rules together. \verb+LDBM+ seems to be a component of the software, by the name presumably a database management component. The variants this component can use, \verb+BTREE+ and \verb+DBHASH+, are different indexing approaches. 

\paragraph{postgres}

Similar to openldap, postgres has also few rules in relation to its size in comparison to openvpn and libxml2. Its average confidence is also about 97\% and the average support is 0,33\% with an minimum support of 0,23\% as the limit.

The rules are also similar to those found in openldap. The developers aren't using a prefix to identify the features as postgres features. Logical connections between features in different rules can still be made however. For example, there are rules combining \verb+HAVE_TM_ZONE+ and \verb+HAVE_INT_TIMEZONE+, both referring to timezones by name.  It is therefore logical, that these features change together. Also \verb+__CYGWIN__+ and \verb+WIN32+ reappear in rules together as this was the case in the rule extracted from libxml2.

\paragraph{linux}

Finally, there is the linux kernel with the highest number of distinct features but far less extracted rules in comparison to openvpn and libxml2. The different numbers have no impact on the ongoing trend of the average confidence. The average confidence for the linux kernel results in 97\% while the average support is very low with 0,016\%. This low support is not surprising, as the number of feature commits and features is far greater than in the other systems, resulting in a far lower support. The minimum support in this case was set to 0,013\%.

Within the linux kernel many rules contain features with the prefix \verb+CONFIG+, which marks different configuration options for building user defined kernels, like the bluetooth configuration for the kernel used in Ubuntu Core for the Raspberry Pi\footnote{Configuration options for Ubuntu Core: \url{https://ubuntu.com/core/docs/bluez/reference/device-enablement/linux-kernel-configuration-options}}. Within these configuration options, the rules contain associated changes between different components are logical. Changes in different parts of the IPv6 component for example are connected with changes in other parts of this component. This is also true for other options like keyboard support or system architecture and reveals logical connections between the features changed together in the association rules.


\section{Answering the Research Questions}\label{ch:results:rq}

After the presentation of the results in the last two sections, in this section, I answer and discuss the research questions stated in \autoref{ch:rq}. 

\textbf{Do co-changes between features occur frequently?}

Out of the total number of commits (\autoref{tab:subjectsystems}) only a subset contains feature changes. These feature commits do not encompass all feature changes on more than one feature, so-to-speak co-changes between features. However, as visualised in \autoref{tab:overview-res-commits}, the number of feature commits with co-changes is considerable. So I conclude that there are frequent co-changes.

The ratio between co-change commits and all feature commits varies over the different subject systems but does not drop below 25\%. This implies that in other systems with feature annotations and existing feature commits, the probability to find co-changes is high. The amount of co-changes might vary and the number of feature commits is no indicator for the number of co-change commits.

The linux kernel in this experiment has the lowest number of feature commits, as only 10\% of the total commits show feature changes and also the lowest number of co-change commits, 25\% out of the feature commits. A smaller system like openvpn has feature changes in nearly 53\% of the commits, with 68\% of them containing co-changes. This shows a big spread of ratios between different systems. With the other systems of different size this can show a trend for larger systems to have a decreasing number of co-change commits. A possible  explanation could be the decreasing amount of feature code in the overall project, but this is only an assumption and would need further investigation.

\textbf{To what extent do co-changes reveal implicit feature dependencies?}

Since I detected frequent co-changes, this could be hinting at implicit feature dependencies, but could also just be a coincidence. To investigate further if there are patterns within these changes, association rule mining was used to identify real dependencies.
 In all systems rules were extracted using the defined thresholds for confidence and support. The average confidence across all systems is above 95\%, while the average support is under 1\%. 

Low support values like this are normal with this type of data. If the same group of features would be changed in 50\% of the commits, resulting in a support level of 50\%, something would not be normal in development. As shown by \citeauthor{7476643}, only few files are typically changed in a commit. With the large number of features (or files in their case) it would be worrying, if the same few would be changed very often while others stay untouched the whole time. This would lead to the impression, that something in this feature is not working properly and the developers try to fix it without success. This would leave the software and their developers standing in a bad light.

Between the different systems, the number of extracted rules varies greatly. The numbers do not reveal a direct trend, like a sinking number of rules with a rising number of commits or features for example. However, for similar feature counts the numbers of extracted rules is also similar, which leads to the assumption of an association between these numbers. For a qualified statement however, further research is needed.

For all rules that have been detected, I could observe that often the participating features also logically belong together. An example would be the different indexing approaches for the database management system found in openldap. But there are also rules - and by that dependencies - that are not logical or obvious for the developer. These are the implicit dependencies that are of great interest for the developers of these systems.


\section{Threats to Validity}

Overall this research has some limitations that could impact the validity of the results. These threats to validity are summarised in the following paragraphs and are divided into internal and external threats.

\paragraph{Internal Threats}

For measuring the size of a repository, I used the number of lines of code as well as the number of commits. These are valid and comparable measurements and often used in similar studies like \cite{10.1007/s10270-015-0483-z, 7335413}, but not the only available ones. Another measure for the size of a repository would be the number of files as a quantitative measurement or the aggregated file size (only of C files). This however would have added an extra level of complexity to the analysis and was out of scope for this work. 

During the data aggregation the commit message (a textual information about the changes by the developer) could have been saved as well. This could have given a greater insight into why features changed together, as the cause for change is often stated in this message. This however is also out of scope and only would have improved the understanding of the results, as to why features change together would hopefully be more intelligible. Nevertheless, with my currently extracted information, I can reliably analyze whether and where co-changes of features occur, which was the main goal of this thesis.

As previously stated, only modified and added lines where analysed. Deleted lines, as recorded in the version control system, where neglected, as their extraction would have doubled the processing time and added multiple steps to the aggregation process. The reason for this is that the line numbers of deleted lines correspond to the previous commits file state. This would have meant, that for every deletion, the parent commits file state would have to be processed for feature locations as well, adding a second analysis step and likely doubling the processing time of one commit.
 This however does not effect the validity of the results, as this only relates to changes where exclusively deletions where executed. In most cases deleted lines are accompanied by added lines, as functionality is not removed completely from a system in the vast majority of commits. 

\paragraph{External Threats}

Externally the validity of my results could be threatened by the limited number of subject systems and the hereby limited range of domains. The results probably can not be generalised to the domain of text editors for example. Due to the small range of domains, my selection is not bias to one specific domain and the results can at least be generalised for similar domains. 


\section{Chapter Summary}

In this chapter the results from the  experiment were presented. In the first section of this chapter, an overview of the subject systems is given by presenting different tables with metrics of features and commits for the systems. The second section then presents the results of the co-change analysis with the numbers presented as an overview, followed by some detailed inspections to identify logical associations between the features in a rule. The chapter is closed by answering the research questions.



