\chapter{Evaluation}\label{ch:eval}

In \autoref{sec:problem} my approach and its implementation were presented. This chapter contains the evaluation of that approach. For this I introduce the research questions I want to answer and the systems I want to investigate. Furthermore, the evaluation procedure will be explained. Closing the chapter, I outline some challenges that had to be solved during the evaluation.


\section{Research Question}\label{ch:rq}

As shown in \autoref{ch:related} the vast majority of research regarding features in configurable software systems has been conducted on snapshots of the codebase. This allows more indepth analysis of code and features but does not utilise the available data from the version control systems as results could be different in different stages of the software development process. 
The few research papers utilising the change data are not focused on (implicit) feature dependencies, instead focusing more on the single features and the code inside them.

In this work I attempt to combine the change data analysis with a feature analysis, to evaluate if rules for implicit dependencies in co-changes can be extracted using the methodology presented in the last chapter. In particular, I want to answer the following research questions with the evaluation:

\begin{enumerate}
    \item \textbf{Do co-changes between features occur frequently?} \\ Even if features in form of preprocessor statements are used in a software project, there don't have to be any changes on two or more features in the same version of the code. However frequent changes on two or more features indicate the possibility for association rules in the data.
    \item \textbf{To what extent do co-changes reveal implicit feature dependencies?} \\ If co-changes exist in the data, these co-change data can be processed with an algorithm for association rule mining. With this I aim to find association rules between those features. But are these associations just appearing randomly together in the commits or are they logically linked, like implicit dependencies?
\end{enumerate}


\section{Subject Systems}

In \autoref{ch:methodic} the general process as well as the used tools are described. Within this chapter the use of cppstats for the analysis is mentioned, a tool used to analyse C code. This limits the selection of possible subject systems to software written in the C programming language and usage of the C preprocessor. Both \citeauthor{10.1007/s10270-015-0483-z} and \citeauthor{7335413} use a number of subject systems for their analysis. The majority of the selected systems are a subset of the ones presented by \citeauthor{10.1007/s10270-015-0483-z}. 

\footnotesize
\begin{tabularx}{\linewidth}{c c c c c c}
	
	\hline
	System & Domain & Lines of Code\footnote{Lines of Code: Counted using cloc - \url{https://github.com/AlDanial/cloc} - on the latest commit analysed. Only counted lines of code in c files.} & \# Commits\footnote{Number of Commits at the time of analysis} & First Commit & Last Commit\footnote{Date of last commit analysed} \\
	\hline
	\hline
	libxml2\footnote{ Repository used for libxml2: \url{https://gitlab.gnome.org/GNOME/libxml2}} & \specialcell{Programming\\Library} & 201 757 & 5 321 & 24.071998 & 03.04.2022\\ \hline
	openvpn\footnote{Repository used for openvpn: \url{https://github.com/OpenVPN/openvpn}} & \specialcell{Network\\Service} & 79 216 & 3 176 & 26.09.2005 & 17.03.2022 \\ \hline
	openldap\footnote{Repository used for openldap: \url{https://git.openldap.org/openldap/openldap/}} & \specialcell{Network\\Service} & 291 726 & 23 985 & 09.08.1998 & 11.04.2022 \\ \hline
	postgres\footnote{Repository used for postgres: \url{https://github.com/postgres/postgres/}} & \specialcell{Database\\System} & 872 412 & 53 695 & 09.07.1996 & 22.03.2022\\ \hline
	linux kernel\footnote{Repository used for linux kernel: \url{https://github.com/torvalds/linux}} & \specialcell{Operating\\System} & 16 143 672 & 1 090 089 & 17.04.2005\footnote{Previous history was not imported into git, as mentioned in first commit: \url{https://github.com/torvalds/linux/commit/1da177e4c3f41524e886b7f1b8a0c1fc7321cac2}} & 02.04.2022 \\ \hline
	
	\caption{Subject systems for the analysis with some additional information}\label{tab:subjectsystems}
\end{tabularx}
\normalsize

The selected systems are from different domains and vary in size, both from the number of lines of code and the number of commits, as can be seen in \autoref{tab:subjectsystems}. This mix is chosen to reduce the bias in the results, injected through system with similar domains or sizes. The table also lists the date of the first commit and the date of the last commit analysed. The date of the first commit is the date of the first commit in the linked git repository. Most of the systems have commits older than 2005, the year git as a version control system was released \cite{git-story}. Other systems, like the linux kernel, copied the current state into the new system as an initial commit and saved the previous history only in the old system.


\section{Methodology of the Experiment}\label{ch:eval:meth}

Using the list of subject systems presented in \autoref{tab:subjectsystems}, the research questions are answered and the process described in \autoref{ch:methodic} is applied for the evaluation. In the following section, I explain the details of how the evaluation is conducted. 

\paragraph{Data Aggregation}

The first step is the data aggregation stage (reference \autoref{ch:methodic:s1}), the extraction of changed features per commit. As previously stated, the different tasks of this stage are executed inside of Docker containers. The steps are visualised in \autoref{fig:methodic:tech:simply-container}. 

In order to process the large number of commits, the execution of all four steps in succession is automated. During the proof of concept phase, tests were conducted using a command line interface (\textit{cli}) build for the continuous integration platform Woodpecker\footnote{Woodpecker CI homepage: \url{https://woodpecker-ci.org/}}. It builds on Docker and can run multiple containers in succession with a shared working directory on the local system\footnote{Documentation of the woodpecker-cli and the exec-command: \url{https://woodpecker-ci.org/docs/cli\#exec}}. The configuration is realised with a YAML file (structured text). 

For production use in the experiment a \textit{workbalancer} was created. This takes a working directory with the version controlled repository and a list of commits to process as input and executes a predefined list of commands in predefined docker images. This behavior is similar to that of woodpecker but improved with parallel execution of multiple commits, a feature missing in woodpecker.

\paragraph{Co-Change Analysis}

My proposed approach consists of two analyses, as explained in \autoref{ch:mehtodic:s2}. Statistic metrics are generated using a python script. After the initial import of the data, the list of features per commit is filtered to remove duplicate features in one commit. This is achieved by storing the list of features in a \textit{set}\footnote{Python documentation on sets: \url{https://docs.python.org/3/tutorial/datastructures.html\#sets}}, a python datatype that holds a list without duplicates. By sorting the commits by their number of distinct features, the commits with the highest distinct feature-count are identified. Also using the number of distinct features, the number of commits with changes in one and commits with changes in two or more features are extracted. Finally, the data is reordered so that a feature points to the commits it was changed in. As a result, I can easily identify the features with the most changes and features with only one and more than one change. 

\paragraph{Association Rule Mining}

In the second analysis, an apriori algorithm implementation is used to extract association rules for co-changing features. The data gets loaded from the intermediate database and the results are displayed in the console as well as written back into the database. This algorithm needs two parameters for the association rule mining: the minimum support and minimum confidence (for detailed explanation see \autoref{ch:mehtodic:s2}). 

Both parameters are normally specified in percent as their respective metric which is explained in \autoref{ch:mehtodic:s2}. The \textit{minimum confidence} in the experiment will be set to 75\%. This means that 3/4 of the commits with changes in feature $A$ also include changes in feature $B$ in order for the rule $A \rightarrow B$ to be considered. The value is chosen as a middle ground between a conservative value like 90\% and a more aggressive value like 50\%. As an example use case for the resulting rules can be a recommendation system, which would recommend possible co-changes, but with enough confidence in order to not annoy the user with too many irrelevant recommendations.

Deviating from the norm, the \textit{minimum support} is selected as an absolute number instead of a percentage in this analysis. The decision was made, because the algorithm was proposed for transactional data in a supermarket and this work focuses on feature data, which is different from the transactional data. The main difference between the datasets is, that transactional data contains frequent item sets (e.g. butter and bread) in more transactions. In the feature data, the frequency of the item sets decreases, as the changed features vary over time. Even worse, a feature changing in nearly every commit, as for example butter in a supermarket transaction, would indicate problems within the system, as this is an abnormal behavior in development.

To fix this, a value of 15, divided by the number of feature commits (commits with changes in features), is. used for the \textit{minimum support}. In order to catch a rule, the involved features have to occur in 15 feature commits . In comparison to the total number of commits in the chosen subject systems, this number represents a very small fraction of commits. It is set high enough to filter out most of the random co-changes but low enough to have possible association rules in the co-changes included.


\section{Challenges and Solutions}\label{ch:challenges}

Throughout the implementation of the process, multiple challenges occured and solutions as well as improvements had to be found. This section provides an overview of these challenges, some background and how they were solved. This is mainly focused on the data aggregation stage as improvements in this stage had a big impact, as the improvements often were multiplied by the number of commits that needed to be processed. Most improvements had the reduction of execution time as the main focus.

The first prototype was built using woodpecker as a tool for the pipeline execution. Using a bash script with a list of commits as an input this was a great prototype, but the execution time, especially for cppstats, was rather long, since cppstats only uses one processing core (or thread on systems using multithreading). This raised the idea that the execution could be way faster if every core would process one commit. The first idea was to split the commits to the number of cores available and run the script in multiple terminal sessions. This did not work, as woodpecker names the spawned container the same and docker does not allow multiple containers with the same name. To overcome this problem, a small program, formerly referred to as \textit{workbalancer}, was written. Here the name of the step also contains the unique commit hash to avoid container name collisions.

This also provided a solution for another problem with the woodpecker approach. If a command inside a container failed and the execution was ended, the container is not deleted, as the user would probably examine the container to evaluate the error. This lead to the complete pipeline failing, as docker blocked every creation of new container because the name was already used for the failed container. The \textit{workbalancer} checks before creating a container if a container with the name exists. If this is the case, the old container is simply deleted. The logs are written to the console, and could be saved or examined this way and the commit hash is written to an extra file for further investigation.

Another problem was the execution time of the analysis with cppstats. This step alone took over two minutes for one commit from the openvpn project. In order to reduce this time, the number of analysed files per commit was reduced by analysing only C files. These are the files containing the feature annotations and so the interesting ones for this analysis. This also reduced the number of files that had to be copied in order to generate the \textit{right} directory structure for \textit{cppstats}. To further improve the speed of this file copy and other file operations, the working directory was moved from the local filesystem to a main memory based filesystem (\textit{tmpfs}\footnote{More information on tmpfs: \url{https://man7.org/linux/man-pages/man5/tmpfs.5.html}}).

One problem also arose within the execution of cppstats. The feature annotations can contain braces to build complex expressions. One of this expressions in openldap lacked a closing brace\footnote{File with the invalid expression in one commit: \url{https://git.openldap.org/openldap/openldap/-/blob/86c216d10cad596eb8ca37bd9263f56b8dc1042a/libraries/libldap/os-ip.c\#L617}} which resulted in cppstats parsing not only the expression but also some of the following source code as an complex expression. This was reported to the cppstats repo\footnote{Link to Github issue: \url{https://github.com/clhunsen/cppstats/issues/22}}, but as a quick fix the file was manually processed for feature changes. This was only a minor problem as this occurred only once within all commits and all subject systems. 

\section{Chapter Summary}


In this chapter the research questions for this work were introduced and explained. These will be answered using the results, presented in the next chapter. This chapter also presents the subject systems that were chosen for the analysis. Following this section the methodology of the experiment is described. This gives a more detailed insight on the execution of the experiment in contrast to the general description of the process in \autoref{ch:methodic}. Especially the selection of the parameters for the association rule mining is to be named. To conclude this chapter a number of challenges and the implemented solutions given. 


















	