\subsection{Prozess}\label{sec:process}

In diesem Abschnitt beschreibe ich die Kriterien, die in Verbindung mit dem Suchterm in den Suchstrategien verwendet werden. Nach den Kriterien stelle ich meine zwei Suchstrategien, gefolgt von der Studienauswahl vor.

\paragraph{Kriterien}\label{sec:criteria}

Für die weiteren Verfahren habe ich die folgenden Kriterien festgelegt. Diese sind aufgeteilt nach Ausschlusskriterien und Einschlusskriterien. Eine Veröffentlichung wird als akzeptiert gesehen, wenn alle Einschlusskriterien und keines der Ausschlusskriterien zutrifft.

Die Einschlusskriterien sind:
\begin{itemize}
  \item \textbf{Veröffentlichung ist begutachtet} \\
  	Es werden nur begutachtete Veröffentlichungen einbezogen, da davon ausgegangen wird, dass durch die Begutachtung die Qualität der Veröffentlichung sichergestellt wird.\\
  	Für dblp bedeutet das, es werden nur \enquote{Journal Articles} und \enquote{Conference and Workshop Papers} berücksichtigt. In der Scopus Datenbank befinden sich nur begutachtete Veröffentlichungen\footnote{\url{https://www.elsevier.com/de-de/solutions/scopus} (zuletzt abgerufen am 14.02.2021)}, sodass hier keine Einschränkung erfolgt. Für die ACM Digital Libary gilt das gleiche. Damit die Suche einheitlich ist, werden außerdem Bücher ausgeschlossen, da für diese bei dblp nicht gewährleistet ist, dass sie begutachtet sind.
  	
  \item \textbf{GitHub-Funktion behandelt} \textit{nur bei Studienauswahl} \\
  Aus dem Abstrakt ist ersichtlich dass die Autoren sich mit der Nutzung von GitHub-Funktionen beschäftigt haben. Dieses Kriterium wird nur in der Studienauswahl (siehe \autoref{sec:selection}) angewandt.
\end{itemize}

Die Ausschlusskriterien sind:
\begin{itemize}
  \item \textbf{Erscheinungsdatum vor 2008} \\ 
	GitHub ging erst 2008 online, somit können vorher keine Veröffentlichungen über ein Feature des Services gemacht worden sein\footnote{\url{https://github.blog/2008-04-10-we-launched/} (zuletzt abgerufen am 14.02.2021)}.
	
  \item \textbf{Sprache ist nicht Deutsch oder Englisch} \\
  	Da ich nur Deutsch und Englisch verstehe, kann ich auch nur Veröffentlichungen in diesen Sprachen in meine Analyse einfließen lassen.
  	
  \item \textbf{Feature ist API}\\
  	Ich beschränke mich in dieser Arbeit auf Features der Weboberfläche und schließe deswegen die programmatorischen Schnittstellen aus.
  	
  \item \textbf{kein neues Tool}\\
  	Es werden in vielen Arbeiten Algorithmen oder Tools vorgestellt oder erstellt, die eine bestimmte Funktion von GitHub erweitern oder verbessern. Da diese Arbeiten aber einen Fokus auf die Funktionsweise und den Nutzen des neuen Tools legen, werde ich diese Arbeiten nicht betrachten.
\end{itemize}


\paragraph{Strategie 1: automatische Suche}\label{sec:autosearch}

\cite{SLRBook} stellen drei Suchstrategien in ihrem Buch vor: automatische Suche, manuelle Suche und Snowballing. Für das Snowballing werden als Ausgangspunkt bereits wissenschaftliche Arbeiten benötigt. Somit eignet es sich nicht als erste Strategie. Ich verwende es jedoch als zweite Strategie und erkläre es im nächsten Abschnitt.

Bei der manuellen Suche werden händisch verschiedene wissenschaftliche Fachzeitschriften, die sich mit dem Themengebiet der Arbeit befassen, durchsucht und relevante Artikel identifiziert. Dieser Prozess ist meist sehr zeitaufwendig und macht dann Sinn, wenn das Themengebiet multidisziplinär ist.

Die automatische Suche funktioniert ähnlich zur manuellen Suche. Hierbei wird aus der Fragestellung zuerst ein Suchterm entwickelt. Dieser wird dann verwendet um in verschiedenen Literaturdatenbanken nach passenden Veröffentlichungen zu suchen. Diese Suche kann noch durch weitere Kriterien eingegrenzt werden. 

Ich habe mich für die automatische Suche als initiale Strategie entschieden, da der Zeitaufwand geringer ist im Vergleich zur manuellen Suche. Dies ist im Anbetracht des begrenzten Zeitrahmens einer Bachelorarbeit sehr relevant. Außerdem sind fast alle Veröffentlichungen in einer der Datenbanken zu finden, sodass die Resultate nicht schlechter ausfallen \citep{SLRBook}.

Die automatische Suche wird auf die drei Datenbanken dblp, Scopus und ACM ausgeführt (siehe \autoref{sec:database}). Der Suchterm wurde in \autoref{sec:term} aufgestellt und erklärt und die Kriterien finden sich in \autoref{sec:criteria}. 


\paragraph{Strategie 2: Snowballing}\label{sec:snowballing}

\cite{SLRBook} stellen zwei Formen des Snowballings vor, das Backwards Snowballing und das Forward Snowballing.

Beim Backwards Snowballing wird für jedes Paper, dass durch die automatische Suche gefunden und durch die Studienauswahl als relevant markiert wurde, die zitierte Literatur durchsucht. Hierbei werden vorher definierte Kriterien verwendet um möglicherweise relevante Literatur zu finden.

Das Forward Snowballing funktioniert ähnlich. Hierbei werden die Veröffentlichungen betrachtet, in denen bereits als relevant markierte Veröffentlichung zitiert wurde.

In dieser Arbeit werden beide Methoden jeweils einmal angewendet, jedoch mit der Einschränkung, dass ein Backwards Snowballing nur für Dokumente durchgeführt wird, die in 2009 oder später veröffentlicht wurden, da GitHub erst 2008 gegründet wurde. Diese Kriterien wurden bereits in \autoref{sec:criteria} definiert. 

Für das Snowballing verwende ich Scopus. Scopus bieten sowohl die Möglichkeit, direkt das referenzierte Dokument aufzurufen um für dieses die Kriterien abzugleichen, als auch eine Übersicht über alle Veröffentlichungen, die in Scopus erfasst sind, in denen die fragliche Veröffentlichung zitiert wurde, zu erstellen. Diese werden als Scopus Suche dargestellt, sodass von dieser direkt die Detailseite der Veröffentlichung geöffnet werden kann.


\paragraph{Studienauswahl}\label{sec:selection}

Das Verfahren zur Studienauswahl wird jeweils auf die Ergebnisse der Suchstrategie angewendet. Das Ziel ist es dabei die relevanten Veröffentlichungen in der Ergebnismenge zu finden. 

Hierfür definiere ich die folgenden zwei Kriteriengruppen. Wenn eines der Ausschlusskriterien zutrifft oder nicht alle Einschlusskriterien erfüllt werden, wird die Veröffentlichung nicht weiter betrachtet. Wenn keins der Ausschlusskriterien erfüllt wird und alle Einschlusskriterien erfüllt sind, wird die Veröffentlichung in die \ac{LS} aufgenommen.